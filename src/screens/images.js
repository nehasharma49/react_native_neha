import { View, Text, Image, StyleSheet } from 'react-native';
import React from 'react';
import CardDetail from './components/cardDetails';

const ImagesNew = () => {
  return (
    <View>
      <Text style={styles.ImageStyle}>images</Text>
      <Image source={require('../../assets/favicon.png')} />

      <CardDetail />
    </View>
  )
};

const styles = StyleSheet.create({

  ImageStyle: {
    width: 300,
    height: 300,
  },

});

export default ImagesNew;