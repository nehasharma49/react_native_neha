import React from "react";
import { Text, StyleSheet, View } from "react-native";

const ThirdComponent = () => {
    const myName = "Neha Sharma";
    return (
        <View>
            <Text style={styles.textStyle}> 3rd  Components {myName};
            </Text>
        </View>
    );

};

const styles = StyleSheet.create({

    textStyle: {
        fontSize : "30px",
    },

});

export default ThirdComponent;