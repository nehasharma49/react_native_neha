import React from "react";
import { Text, StyleSheet, View } from "react-native";

const CustomComponent = () => {
    return (
        <View>
        
            <Text style={styles.textStyle}>2nd  Components.
            </Text>
        </View>
    );

};

const styles = StyleSheet.create({

    textStyle: {
        fontSize : "30px",
    },

});

export default CustomComponent;