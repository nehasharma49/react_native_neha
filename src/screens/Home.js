import { View, Text, StyleSheet, Button } from 'react-native'
import React from 'react'


const Home = ({ route, navigation }) => {
    const { myName } = route.params;
    return (
        <View style={styles.mainContainer}>
            <Text style={styles.mainHeader}> Welcome  {myName}  </Text>
            <Button title="Go Back" onPress={() => navigation.goBack()} />
        </View>
    )
}

const styles = StyleSheet.create({
    mainContainer: {
        height: '100%',
        paddingHorizontal: 30,
        paddingTop: 30,
        backgroundColor: '#fff'
    },
    mainHeader: {
        fontSize: 25,
        color: '#344055',
        fontWeight: '500',
        paddingTop: 20,
        paddingBottom: 15,
        textTransform: 'capitalize',
    },
    description: {
        fontSize: 20,
        color: '#7d7d7d',
        paddingBottom: 20,
        lineHeight: 25,
        // fontFamily: 'regular',
    },
    inputContainer: {
        marginTop: 20,
    },
    labels: {
        fontSize: 18,
        color: '#7d7d7d',
        marginTop: 10,
        marginBottom: 5,
        lineHeight: 25,
        // fontFamily: 'regular',
    },
    inputStyle: {
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.3)',
        paddingHorizontal: 15,
        paddingVertical: 7,
        borderRadius: 1,
        // fontFamily: 'regular',
        fontSize: 18
    },
    wrapper: {
        // paddingHorizontal: 10,
        // paddingVertical: 15,
        // paddingBottom: 30
        flexDirection: "row",
        // justifyContent:'center',
        alignItems: "center",
        marginTop: 15,
        marginBottom: 360
        // alignContent:"center"

    },
    wrapperText: {
        // paddingLeft: 30
        marginTop: 0,
    },
    buttonStyle: {
        borderRadius: 40,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: -300

    },
    buttonText: {
        color: '#fff',
        fontSize: 20,
        justifyContent: 'center',
        alignContent: 'center',
        fontWeight: '600'

    },
});

export default Home