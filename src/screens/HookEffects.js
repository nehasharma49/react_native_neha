import { View, Text, StyleSheet } from 'react-native'
import React, {useEffect} from 'react'

const HookEffects = () => {

    const getUserData = async () => {
        try{
          const response = await fetch("https://thapatecnical.github.io/userapi/user.json");

         const myData = await response.json();
         console.log(myData);

        } catch (error){
            console.log(error);
        }


    };

    useEffect(() => {getUserData();
    }, []);
    
  return (
    <View>
      <Text>HookEffects</Text>
    </View>
  );
};

const styles = StyleSheet.create({


});

export default HookEffects;