import { FlatList, Text, View } from 'react-native';
import React from 'react';

const FlatListNew = () => {
    const names = [
        {
            index: "1",
            name: 'Neha',
        },
        {
            index: "2",
            name: 'Jayshree',
        },
        {
            index: "3",
            name: 'Anil',
        },
        {
            index: "4",
            name: 'Amit',
        },
    ];

    return <FlatList keyExtractor={(key) => {
        return key.index;
    }}
      horizontal
        data={names} renderItem={(element) => {

            console.log(element);
            return <Text> {element.item.name} </Text>;

        }} />;



};

export default FlatListNew;