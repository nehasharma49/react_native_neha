import { View, Text, StyleSheet, TextInput, TouchableOpacity, Alert } from 'react-native'
import React, { useState } from 'react'
import Checkbox from 'expo-checkbox'


const ContactUs = ({navigation}) => {

    const [userName, setUserName] = useState("");
    const [password, setPassword] = useState("");
    console.log(userName, password);

    const submit = () => {
        // return Alert.alert(userName, password);
        if(userName === "neha" && password === "sharma"){
            Alert.alert(`Thank You ${userName}`);
            navigation.navigate("Home", {myName: `${userName}`});

        }else{
            Alert.alert(`Username and Password is not correct`);
        }
    };

    const [agree, setAgree] = useState(false);
    return (
        <View style={styles.mainContainer}>
            <Text style={styles.mainHeader}>Login Form</Text>
            <Text style={styles.description}>You can reach us via any@s.com</Text>
            <View style={styles.inputContainer}>
                <Text style={styles.labels}>Enter Your Name</Text>
                <TextInput style={styles.inputStyle}
                    autoCapitalize="none"
                    autoCorrect={false}
                    value={userName}
                    onChangeText={(actualData)=> setUserName(actualData)}
                />
            </View>

            <View style={styles.inputContainer}>
                <Text style={styles.labels}>Enter Your Password</Text>
                <TextInput style={styles.inputStyle}
                    autoCapitalize="none"
                    autoCorrect={false}
                    secureTextEntry={true}
                    value={password}
                    onChangeText={(actualData)=> setPassword(actualData) }
                />
            </View>

            <View style={styles.wrapper}>
                <Checkbox
                    value={agree}
                    onValueChange={() => setAgree(!agree)}
                    color={agree ? "#4630EB" : undefined}


                />

                <Text style={styles.wrapperText}>I have read and agreed with the TC</Text>
            </View>

            <TouchableOpacity
                style={[styles.buttonStyle,
                {
                    backgroundColor: agree ? "#4630EB" : "grey",
                }
                     ]}
                     disabled={!agree}
                     onPress={()=> submit()}
                     >
                <Text style={styles.buttonText}>Login</Text>
            </TouchableOpacity>

        </View>
    )
}

const styles = StyleSheet.create({
    mainContainer: {
        height: '100%',
        paddingHorizontal: 30,
        paddingTop: 30,
        backgroundColor: '#fff'
    },
    mainHeader: {
        fontSize: 25,
        color: '#344055',
        fontWeight: '500',
        paddingTop: 20,
        paddingBottom: 15,
        textTransform: 'capitalize',
    },
    description: {
        fontSize: 20,
        color: '#7d7d7d',
        paddingBottom: 20,
        lineHeight: 25,
        // fontFamily: 'regular',
    },
    inputContainer: {
        marginTop: 20,
    },
    labels: {
        fontSize: 18,
        color: '#7d7d7d',
        marginTop: 10,
        marginBottom: 5,
        lineHeight: 25,
        // fontFamily: 'regular',
    },
    inputStyle: {
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.3)',
        paddingHorizontal: 15,
        paddingVertical: 7,
        borderRadius: 1,
        // fontFamily: 'regular',
        fontSize: 18
    },
    wrapper: {
        // paddingHorizontal: 10,
        // paddingVertical: 15,
        // paddingBottom: 30
        flexDirection: "row",
        // justifyContent:'center',
        alignItems: "center",
        marginTop: 15,
        marginBottom: 360
        // alignContent:"center"

    },
    wrapperText: {
        // paddingLeft: 30
        marginTop: 0,
    },
    buttonStyle: {
        borderRadius: 40,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: -300

    },
    buttonText: {
        color: '#fff',
        fontSize: 20,
        justifyContent: 'center',
        alignContent: 'center',
        fontWeight: '600'
        
    },
});

export default ContactUs