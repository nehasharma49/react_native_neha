import React from "react";
import { Text, StyleSheet, View } from "react-native";
import ContactUs from "./src/screens/ContactUs";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Home from "./src/screens/Home";


const App = () => {
  const stack = createNativeStackNavigator();
  
  return (
  <NavigationContainer>
  <stack.Navigator initialRouteName="Login">
    <stack.Screen name="Login" component={ContactUs}/>
    <stack.Screen name="Home" component={Home}/>
  </stack.Navigator>
  </NavigationContainer>
  );
}

  

  export default App;